# Instalar requerimentos:

``` pip install -r requirements.txt ```

# Rotas:

> API rodando em 10.10.0.29:5005

> Repo:https://gitlab.com/Natanael.gonzales/group-devops/-/tree/main/apiBeer?ref_type=heads

* Entrada com número inteiro fixo:

``` http://10.10.0.29:5005/api/beer/{INT} ```

> São exibidas as primeiras {INT} cervejas.

* Uma cerveja aleatória:

``` http://10.10.0.29:5005/api/beer/random ```

>  Para entrada aleatória, o número de cervejas retornadas é fixo e é exibido o primeiro valor retornado.

