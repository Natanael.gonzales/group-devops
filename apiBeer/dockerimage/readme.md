# Buildar a imagem:

* Buildar imagem sem cache para garantir que seja atualizada:

``` docker build -t beer-api . --no-cache ```

* Rodar a imagem bindando a porta 5005 do container com a porta 5005 do host e em background:

``` docker run -p 5005:5005 -d --name beer-container beer-api ```

> É importante, o EXPOSE do Dockerfile não garante que a porta seja exposta, é necessário utilizar o -p na hora de rodar o container.
