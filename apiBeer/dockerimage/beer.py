# 4) Crie um script (na linguagem de sua preferência) que consulte a URL https://random-data-api.com/api/v2/beers?size=100 (essa API retorna dados aleatórios, retornando 100 elementos). Crie uma API simples para retornar os dados retornados por essa chamada. A sua API deverá ter um método para retornar apenas um elemento e outro método que deverá receber um parâmetro inteiro correspondente ao número de elementos a serem retornados.

# - Implantar essa simples API em um dos servidores fornecidos
# - Salvar o código na sua solução de versionamento de código de preferência (github, gitlab, etc)

# Informar: 
# - a URL pública do repositório onde estão os arquivos de definição
# - Em qual VM a API está executando e como chamá-la

from flask import Flask, jsonify
import requests

app = Flask(__name__)
app.json.sort_keys = False # sort keys pra falso pra manter a ordem original dos elementos

# Função para obter os dados da URL
def get_beer_data():
    url = "https://random-data-api.com/api/v2/beers?size=100"
    response = requests.get(url)
    data = response.json()
    return data

# Método para retornar um elemento aleatório
@app.route('/api/beer/random', methods=['GET'])
def get_random_beer():
    beer_data = get_beer_data()
    random_beer = beer_data[0]  # Retorna o primeiro elemento
    return jsonify(random_beer)
 
# Método para retornar um número específico de elementos
@app.route('/api/beer/<int:num_elements>', methods=['GET'])
def get_multiple_beers(num_elements):
    if num_elements > 100 or num_elements < 1:
        return "Numero inválido de elementos. Por favor, insira um número entre 1 e 100."
    beer_data = get_beer_data()
    return jsonify(beer_data[:num_elements])

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5005)
