SELECT 'CREATE DATABASE teste_group' 
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname='teste_group') \gexec
\c teste_group

CREATE TABLE IF NOT EXISTS schemainfo(
    tech varchar(255),
    tablename varchar(255),
    columnname varchar(255),
    PRIMARY KEY(tech, tablename, columnname)
);

CREATE TABLE IF NOT EXISTS elementinfo(
    tech varchar(255),
    elementname varchar(255),
    elementid varchar(255),
    PRIMARY KEY(elementid)
);
