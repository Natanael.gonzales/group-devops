# Criando uma imagem personalizada com Dockerfile

* Em qual host o container foi criado: 10.10.0.29
* O arquivo de definição usado para construir a imagem personalizada:
    - Dockerfile (repo https://gitlab.com/Natanael.gonzales/group-devops/-/tree/main/customImage?ref_type=heads)

- Qual(is) porta1(s) foi(ram) publicada(s) e como acessar a aplicação disponibilizada: Seguir as instruções abaixo.


## Buildar a imagem

* Buildar imagem sem cache para garantir que seja atualizada:

``` docker build -t custom-postgres . --no-cache ```

> Neste exemplo crio uma imagem personalizada para o postgres e baixo do docker hub a imagem do nginx.

* Rodar o docker compose para subir os containers:

``` docker compose up -d ```

## Testando os containers

* Acessar o container do postgres:

O banco de dados está exposto na porta 5432. Para acessar o banco de dados, utilize um conector de banco de dados como o DBeaver. Os usuários e senhas estão definidos no arquivo docker-compose.yml.

* Acessar o container do nginx:

O servidor nginx está exposto na porta 80. Para acessar o servidor web, abra um navegador e acesse [página main](http://10.10.0.29:80/main.html) (página teste criada apenas para expor o conteúdo).
