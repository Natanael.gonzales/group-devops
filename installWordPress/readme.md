2) Considere o seguinte cenário: 
Você precisa subir uma aplicação baseada em containers, com pelo menos 4 instâncias, sendo que um deles deve obrigatoriamente atuar como um proxy e dois devem ser réplicas redundantes entre sim. Apenas o proxy poderá ter portas de rede publicadas para acessar a aplicação e pelo menos um dos containers deverá ser criado a partir de uma imagem personalizada.

- Criar o arquivo de definição da imagem personalizada

- Criar, usando a ferramenta de sua preferência (Github actions, Gitlab CI/CD, Jenkins, etc), um pipeline automatizado que seja capaz de fazer o build de uma imagem de container a partir do arquivo de definição criado

- Fazer a implantação da arquitetura solicitada de preferência usando alguma solução de orquestração simples (single host)

- Os arquivos utilizados neste cenário deverão ser disponibilizadas na sua solução de versionamento de código de preferência (github, gitlab, etc)

Sugestão de aplicação web: wordpress

----------

- Como tive alguns problemas de conexão nas instâncias, não consegui desenvolver esta solução em tempo hábil para a entrega, mas segue minha estratégia:

1) Criar um arquivo de definição da imagem personalizada do nginx com as configurações necessárias para atuar como proxy reverso.

2) Criar um arquivo de definição da imagem personalizada do wordpress com as configurações necessárias para atuar como aplicação web.

3) Criar um arquivo de definição do docker-compose.yml para subir a aplicação baseada em containers, com 2 replicas do wordpress, 1 do banco de dados e 1 do nginx.

4) Criar um pipeline automatizado que seja capaz de fazer o build de uma imagem de container a partir do arquivo de definição criado.

``` # .gitlab-ci.yml

stages:
  - build

variables:
  IMAGE_TAG: latest
  IMAGE_NAME: nginx-wp

build_image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $IMAGE_NAME:$IMAGE_TAG
  only:
    - main

```
