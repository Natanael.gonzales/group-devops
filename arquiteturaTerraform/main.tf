# Provedor de serviços em nuvem
provider "aws" {
  region = "us-east-1"  # Região
}

# Recursos
module "web_servers" {
  source = "./modules/web_servers"
}

module "app_servers" {
  source = "./modules/app_servers"
}

module "db_servers" {
  source = "./modules/db_servers"
}

module "load_balancer" {
  source = "./modules/load_balancer"
}

module "ssl_certificate" {
  source = "./modules/ssl_certificate"
}

module "security_group" {
  source = "./modules/security_group"
}

# Regras de segurança para acesso administrativo
resource "aws_security_group_rule" "admin_access" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["200.201.202.203/32"]
  security_group_id = module.security_group.id
}
