# Componentes da Arquitetura:

## Camada Web:

* Dois servidores web (EC2 instances) configurados para lidar com solicitações HTTP/HTTPS.

* Um balanceador de carga (Load Balancer) à frente dos servidores web para distribuir o tráfego de entrada.

* Um certificado SSL/TLS público publicado para garantir a comunicação segura.

* Configurações de segurança para permitir acesso público somente às portas HTTP/HTTPS.

## Camada de Aplicação:

* Dois servidores de aplicação (EC2 instances) para processar a lógica de negócios da aplicação.
* Configurações de segurança para permitir acesso somente a partir da camada web.

## Camada de Banco de Dados:

* Dois servidores de banco de dados (EC2 instances) para armazenar dados da aplicação.
* Configurações de segurança para permitir acesso somente a partir da camada de aplicação.

## Outros Componentes:

* Um security group para definir regras de acesso e restrições de tráfego entre as camadas.
* Um certificado SSL/TLS gerenciado pelo AWS Certificate Manager (ACM) para comunicação segura entre o cliente e os servidores web.


## Estrutura de Arquivos:

```
main.tf: Define os recursos principais da infraestrutura, incluindo instâncias EC2, balanceador de carga, certificado SSL/TLS e grupo de segurança.
modules/web_servers/main.tf: Define os servidores web e suas configurações.
modules/app_servers/main.tf: Define os servidores de aplicação e suas configurações.
modules/db_servers/main.tf: Define os servidores de banco de dados e suas configurações.
modules/load_balancer/main.tf: Define o balanceador de carga e seu target group.
modules/ssl_certificate/main.tf: Define o certificado SSL/TLS usando o AWS Certificate Manager (ACM).
modules/security_group/main.tf: Define o grupo de segurança e suas regras de acesso.
```

Observações:

Os arquivos de definição estão escritos em HCL para uso com o Terraform.
Substitua os valores específicos, como IDs de subnets, VPCs, AMIs.
