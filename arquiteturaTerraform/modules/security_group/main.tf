# Recurso para o grupo de segurança
resource "aws_security_group" "security_group" {
  name        = "security-group"
  description = "Security group for the application architecture"
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Acesso público para a camada web
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "id" {
  value = aws_security_group.security_group.id
}
