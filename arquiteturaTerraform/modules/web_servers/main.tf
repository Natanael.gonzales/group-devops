# Recursos para servidores web
resource "aws_instance" "web_servers" {
  count         = 2
  ami           = "ami-0c55b159cbfafe1f0"  # ID da AMI do sistema operacional desejado
  instance_type = "m5.xlarge"
  # Outras configurações (4vCPUs, 32GB de memória, 100GB de disco SSD) podem ser definidas aqui
}

# Configurações adicionais para os servidores web
# Implementar as filas de mensagens para comunicação entre os servidores web e de aplicação aqui
