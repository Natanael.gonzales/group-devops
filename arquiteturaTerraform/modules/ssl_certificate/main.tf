# Recurso para o certificado SSL/TLS
resource "aws_acm_certificate" "ssl_certificate" {
  domain_name       = "example.com"
  validation_method = "DNS"

  tags = {
    Name = "SSL Certificate"
  }
}
