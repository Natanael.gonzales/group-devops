# Recursos para servidores de aplicação
resource "aws_instance" "app_servers" {
  count         = 2
  ami           = "ami-0c55b159cbfafe1f0"  # ID da AMI do sistema operacional desejado
  instance_type = "m5.xlarge" # tipo da instância
  # Outras configurações (4vCPUs, 32GB de memória, 100GB de disco SSD) podem ser definidas aqui
}
