# Instala Apache e Mysql 8

## Instalar Ansible

* Script para instalar Ansible e criar um virtual environment:
``` sudo chmod +x install-Ansible.sh && ./install-Ansible.sh ```

* Ativar virtual environment:
``` source ansible-venv/bin/activate ```

* Instalar dependências:
``` ansible-galaxy install -r requirements.yml ```

* A variável mysql_installers_path_list: /home/natanael/Downloads deve ser substituída pelo caminho onde o arquivo mysql-apt-config_0.8.22-1_all.deb estiver localizado.

> O arquivo mysql-apt-config_0.8.22-1_all.deb pode ser baixado em https://repo.mysql.com//mysql-apt-config_0.8.22-1_all.deb e deve estar no diretório acima mencionado, no host onde será instalado - enviar por scp ou `curl -O https://repo.mysql.com/mysql-apt-config_0.8.22-1_all.deb`


* Executar playbook:
``` ansible-playbook installApacheandMysql/playbook.yml -K ```

> -K solicita entrada do become password.

* Verificar versão do MySQL:

``` /usr/bin/mysql --version ```
