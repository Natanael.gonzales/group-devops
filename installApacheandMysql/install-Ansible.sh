#!/bin/bash

# Checa se python 3 está instalado
if ! [ -x "$(command -v python3)" ]; then
    echo "Instalando o Python 3..."
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        if [ $ID = "ubuntu" ]; then
            sudo apt-get update
            sudo apt-get install -y python3
        elif [ $ID = "centos" ]; then
            sudo yum install -y python3
        else
            echo "Distribuição Linux não suportada."
            exit 1
        fi
    else
        echo "Distribuição Linux não suportada."
        exit 1
    fi    
fi

# Checa se pip está instalado
if ! [ -x "$(command -v pip)" ]; then
    echo "Instalando o pip..."
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        if [ $ID = "ubuntu" ]; then
            sudo apt-get update
            sudo apt-get install -y python3-pip
        elif [ $ID = "centos" ]; then
            sudo yum install -y python3-pip
        else
            echo "Distribuição Linux não suportada."
            exit 1
        fi
    else
        echo "Distribuição Linux não suportada."
        exit 1
    fi    
fi

echo "Instalando o Python Venv..."
    . /etc/os-release
    if [ $ID = "ubuntu" ]; then
        sudo apt-get update
        sudo apt-get install -y python3-venv
    fi

# Criar o ambiente virtual ansible-venv
echo "Criando o ambiente virtual ansible-venv..."
python3 -m venv ansible-venv

# Ativar o ambiente virtual ansible-venv
echo "Ativando o ambiente virtual ansible-venv..."
source ansible-venv/bin/activate

# Instalar o Ansible
echo "Instalando o Ansible..."
pip install ansible

# Verificar se a instalação foi bem-sucedida
echo "Verificando a instalação do Ansible..."
ansible --version
which ansible

echo "O ambiente virtual ansible-venv foi criado e o Ansible foi instalado com sucesso."
