Durante a execução do desafio, encontrei dificuldades na comunicação entre as instâncias fornecidas. Inicialmente, ao desenvolver a solução localmente para posterior implantação nas instâncias (considerando o horário - já era noite), investi algum tempo tentando resolver o problema de comunicação entre elas. Suspeitei que o problema estivesse relacionado à minha solução em desenvolvimento. No entanto, após revisar minhas configurações, percebi que não encontraria a solução dessa maneira.

Posteriormente, entrei em contato com o Luiz para informar sobre minhas tentativas de solucionar o problema e que, até o momento, não obtive sucesso. Sugeri que a causa provável do problema pudesse estar relacionada à configuração da VPC na AWS, porém, no final, se tratava de uma questão relacionada ao Security Group.

Para tentar solucionar o problema, adotei as seguintes estratégias de troubleshooting:

Verifiquei se todas as instâncias estavam em execução, e se eram acessíveis a partir do host Windows.

Confirmei se as portas necessárias estavam abertas nas instâncias e não identifiquei nenhuma porta bloqueada.

Verifiquei se as configurações de rede das instâncias estavam corretas, comparando-as com as configurações da máquina Windows que estava funcionando. Confirmei que os adaptadores estavam configurados corretamente com DNS e DHCP funcionando conforme esperado.

Garanti que não houvesse políticas internas nas instâncias bloqueando conexões, verificando que os arquivos hosts.allow e hosts.deny estavam vazios.

Confirmei que o firewall estava desativado em todas as instâncias.

Utilizei o comando traceroute para verificar as configurações de rota das instâncias e observei que nenhuma conexão saía delas.

Confirmei que não havia configurações de proxy configuradas nas instâncias.
